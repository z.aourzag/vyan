const leon = '463800854210805771';
const askara = '305407776199540736';
const { Monitor, util } = require('klasa');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			name: 'leon',
			enabled: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (msg.author.id !== leon && msg.author.id !== this.client.owner.id && msg.author.id !== askara) return;
		if (msg.content.toLowerCase().replace(/\W/, '').replace("'", '').replace(/\s/, '') === 'imsorry') {
			return msg.author.send('gitlab.com/Nekiono/task_2');
		}

		if (msg.content === 'ily, R') {
			const dm = await msg.author.createDM();
			await this.client.owner.send('passed stage 3');
			dm.startTyping();
			await util.sleep(3000);
			dm.stopTyping(true);
			await dm.send('I love you too.');
			dm.startTyping();
			await util.sleep(10000);
			dm.stopTyping(true);
			await dm.send(['Remember these?', '', 'http://telegra.ph/Nekionoend-08-19', 'http://telegra.ph/Nekionoend-v2-09-15'].join('\n'));
			dm.startTyping();
			await util.sleep(20000);
			dm.stopTyping(true);
			await dm.send('Well, here comes the successor.');
			await util.sleep(500);
			return dm.send('https://telegra.ph/Nekionoend-v3-10-16');
		}
	}

};

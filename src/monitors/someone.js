const { Monitor } = require('klasa');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			name: 'someone',
			enabled: true,
			ignoreSelf: true,
			ignoreBots: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (!msg.guild) return null;
		if (msg.guild.settings.disableSomeone) return null;
		if (!msg.content.includes('@someone')) return null;
		else return msg.channel.send(`_**@someone** ${msg.guild.members.filter(member => !member.user.bot).random()}_`);
	}

};

const { Monitor } = require('klasa');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			enabled: true,
			ignoreBots: true,
			ignoreSelf: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (!msg.guild) return null;
		const { settings } = msg.author;
		const { experience, level } = msg.author.settings;
		const expGained = await this.generateExperience(msg);
		const nextLevel = 100 * ((level + 1) ** 2);
		if (experience + expGained >= nextLevel) {
			await settings.update('level', level + 1);
			for (const role of msg.guild.settings.levelroles) {
				if (role.level <= level + 1) await msg.member.roles.add(role.id);
			}
		}
		return settings.update('experience', experience + expGained);
	}

	generateExperience(msg) {
		const { content } = msg;
		return Math.floor(content.length / 10);
	}

};

const { Monitor } = require('klasa');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			enabled: true,
			ignoreSelf: true,
			ignoreBots: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (!msg.guild || !msg.guild.settings.thepurge.started) return null;
		if (!msg.channel.id === msg.guild.settings.thepurge.channel) return null;
		if (msg.content.toLowerCase().replace(' ', '').includes(msg.guild.settings.thepurge.textToStay.toLowerCase().replace(' ', ''))) {
			await msg.guild.settings.update('thepurge.kickThese', msg.author.id, { action: 'remove' });
			return msg.responder.success(`${msg.author}, you will remain on ${msg.guild.name}.`);
		}
		return null;
	}

};

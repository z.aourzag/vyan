const { Event } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Event {

	async run(message) {
		if (message.partial) await message.fetch();
		if (message.command && message.command.deletable) for (const msg of message.responses) msg.delete();
		if (message.author.bot) return;
		const { settings: { log: { messages: messageLog } } } = message.member.guild;
		if (!messageLog) return;
		const channel = message.guild.channels.get(messageLog);
		const embed = new MessageEmbed()
			.setTitle(`message deleted/${message.author.tag}`)
			.setDescription(message.content)
			.setFooter(`in #${message.channel.name}`)
			.setColor(0xf44336);
		if (channel) channel.send({ embed });
	}

};

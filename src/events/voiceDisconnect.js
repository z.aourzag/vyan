const { Event } = require('klasa');

module.exports = class extends Event {

	run(state) {
		const channel = state.guild.settings.voice.channels.find(ch => ch.id === state.channel.id);
		if (!channel || !channel.owner === state.id) return;

		const date = new Date(Date.now());
		date.setMinutes(date.getMinutes() + 2);
		return this.client.schedule.create('voiceDelete', date, {
			data: {
				channel: state.channel.id
			}
		});
	}

};

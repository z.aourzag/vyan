const phoneticAlphabet = [
	'Alpha',
	'Bravo',
	'Charlie',
	'Delta',
	'Echo',
	'Foxtrot',
	'Golf',
	'Hotel',
	'India',
	'Juliett',
	'Kilo',
	'Lima',
	'Mike',
	'November',
	'Oscar',
	'Papa',
	'Quebec',
	'Romeo',
	'Sierra',
	'Tango',
	'Uniform',
	'Victor',
	'Whiskey',
	'X-ray',
	'Yankee',
	'Zulu'
];

const { Event } = require('klasa');

module.exports = class extends Event {

	constructor(...args) {
		super(...args, {
			name: 'voiceConnect',
			enabled: true,
			event: 'voiceConnect'
		});
	}

	async run(state) {
		await this.checkRejoin(state);
		const temporaryChannel = await state.guild.settings.get('voice.temporary');
		// Database Shit..
		const newStateChannel = state.channel ? state.channel.id : null;

		if (newStateChannel === temporaryChannel) {
			/**
             * Add a check to check the database if the user has one already.
             */

			const channel = await state.guild.channels.create(`${state.member.user.username}: Squad ${phoneticAlphabet[Math.floor(Math.random() * phoneticAlphabet.length)]}`, {
				type: 'voice',
				parent: state.channel.parentID,
				overwrites: [{
					id: state.member.id,
					allow: ['PRIORITY_SPEAKER', 'CONNECT', 'SPEAK', 'VIEW_CHANNEL']
				}]
			});

			await channel.updateOverwrite(this.client.user.id, {
				VIEW_CHANNEL: true,
				CONNECT: true
			})
				.then(_channel => this.client.emit('log', _channel.permissionOverwrites.get(this.client.user.id)))
				.catch(() => null);

			await channel.updateOverwrite(state.guild.id, {
				VIEW_CHANNEL: false,
				CONNECT: false
			})
				.then(_channel => this.client.emit('log', _channel.permissionOverwrites.get(state.guild.id)))
				.catch(() => null);

			/**
             * Database stuff, insert their channel id, owner id, and status of the channel.
             */
			await state.guild.settings.update('voice.channels', {
				id: channel.id,
				owner: state.member.user.id,
				state: true
			});
			return state.member.setVoiceChannel(channel);
		}
	}

	async checkRejoin(state) {
		const channel = state.guild.settings.voice.channels.find(ch => ch.id === state.channel.id);
		if (!channel || !channel.owner === state.id) return;

		const sched = this.client.schedule.tasks.find(task => task.data.channel === state.channel.id);
		if (sched) return sched.delete();
	}

};

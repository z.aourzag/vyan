const { Event } = require('klasa');

module.exports = class extends Event {

	run(channel) {
		if (!channel.type === 'text' || !channel.guild || channel.deleted) return;
		const { muteRole } = channel.guild.settings;
		if (!muteRole) return;
		channel.updateOverwrite(
			muteRole,
			{
				SEND_MESSAGES: false,
				ATTACH_FILES: false,
				ADD_REACTIONS: false
			},
			'Initializing mute-command'
		);
	}

};

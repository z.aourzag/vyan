const { Extendable, Usage, TextPrompt } = require('klasa');
const { Message } = require('discord.js');

module.exports = class extends Extendable {

	constructor(...args) {
		super(...args, { appliesTo: [Message] });
	}

	async createPrompt(question, usageString) {
		const usage = new Usage(this.client, usageString, ' ');
		const prompt = new TextPrompt(this, usage);
		return prompt.run(question);
	}

};

const { Extendable } = require('klasa');
const { User } = require('discord.js');

module.exports = class extends Extendable {

	constructor(...args) {
		super(...args, { appliesTo: [User] });
	}

	get donator() {
		return this.client.guilds.get('367759499613962240').roles.find(role => role.name === 'Donator').members.keyArray().includes(this.id);
	}

};

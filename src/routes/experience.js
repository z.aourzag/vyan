const { Route } = require('klasa-dashboard-hooks');

module.exports = class extends Route {

	constructor(...args) {
		super(...args, { route: 'users/experience/:userID' });
	}

	get(request, response) {
		const { userID } = request.params;
		const user = this.client.users.get(userID);
		if (!user) response.end('{}');
		const { level, reputation, experience } = user.settings;
		const exp = {
			level,
			reputation,
			experience
		};
		return response.end(JSON.stringify(exp));
	}

};

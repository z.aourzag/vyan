const { Task } = require('klasa');

module.exports = class extends Task {

	async run({ guild, user }) {
		const _guild = this.client.guilds.get(guild);
		const member = _guild.members.get(user);
		await member.settings.update('muted', false);
		return member.roles.remove(_guild.settings.muteRole, 'Automatic unmute (temporary mute)').catch(() => null);
	}

};

const { Task } = require('klasa');
const { emojis, colors } = require('../lib/util/constants');
const moment = require('moment');

module.exports = class extends Task {

	async run({ channel, message, author, price, winners }) {
		const _channel = this.client.channels.get(channel);
		const msg = await _channel.messages.fetch(message).catch(() => null);
		if (!msg) return;
		const pool = await msg.reactions.get('🎉').users.fetch().then(r => r.filter(u => !u.bot && msg.guild.members.has(u.id)));
		if (winners > pool.size) winners = pool.size;
		if (pool.size === 0) return _channel.send(`${emojis.error} Yikes! Noone won the \`${price}\`.`);
		const _winners = [];
		for (let i = 0; i < winners; i++) {
			const winner = pool.random();
			pool.delete(winner.id);
			_winners.push(winner);
		}
		await msg.edit({embed: {
			title: '🎊 Giveaway has ended! 🎊',
			description: price,
			color: colors.info
		}})
		return _channel.send(`🎉 Congrats to ${_winners.map(u => u.toString()).join(', ')} on winning the \`${price}\`!`);
	}

};

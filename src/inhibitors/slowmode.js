const { Inhibitor, RateLimitManager } = require('klasa');

module.exports = class extends Inhibitor {

	constructor(...args) {
		super(...args, { spamProtection: true });
		this.slowmode = new RateLimitManager(1, this.client.options.slowmode);
		this.aggressive = this.client.options.slowmodeAggressive;

		if (!this.client.options.slowmode) this.disable();
	}

	async run(message) {
		if (message.author === this.client.owner) return;

		const rateLimit = this.slowmode.acquire(message.author.id);

		try {
			rateLimit.drip();
		} catch (err) {
			const remaining = Math.ceil(rateLimit.remainingTime / 1000);
			if (this.aggressive) rateLimit.resetTime();
			message.delete().catch(() => null);
			message.responder.error(`Sorry **${message.author.tag}**, you must wait ${remaining} second${remaining === 1 ? '' : 's'} to use this command again.`)
				.then(msg => msg.delete({ timeout: rateLimit.remainingTime })).catch(() => null);
			throw true;
		}
	}

};

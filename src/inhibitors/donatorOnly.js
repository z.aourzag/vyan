const { Inhibitor } = require('klasa');

module.exports = class extends Inhibitor {

	constructor(...args) {
		super(...args, {
			enabled: true
		});
	}

	async run(message, command) {
		if (message.author.id === this.client.owner.id) return;
		if (command.donatorOnly && !message.author.donator) throw 'You need to be a donator to use this command.';
		return;
	}

};

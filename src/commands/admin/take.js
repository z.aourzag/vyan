const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Take away credits from someone because you\'re a tyrannic deity.',
			usage: '<someone:user> <amount:int>',
			usageDelim: ' '
		});
	}

	async run(msg, [user, amount]) {
		if (user.bot) return msg.reactor.error();
		await user.settings.update('credits', user.settings.credits - amount);
		return msg.reactor.success();
	}

};

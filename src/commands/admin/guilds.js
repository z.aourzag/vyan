const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			aliases: ['listservers', 'serverlist', 'listguilds', 'guildlist', 'servers'],
			description: 'Get a list of the guilds the bot is on.'
		});
	}

	async run(msg) {
		return msg.responder.info(`${this.client.user.username} is on ${this.client.guilds.size} guilds:`, this.client.guilds.map(g => g.name).sort().join('\n'));
	}

};

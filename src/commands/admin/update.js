const { Command } = require('klasa');
const git = require('simple-git/promise');
const remote = `https://${process.env.GITLAB_USER}:${process.env.GITLAB_PASS}@${process.env.GITLAB_REPO}`;

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			guarded: true,
			description: 'Apply an update from the repo.'
		});
	}

	async run(msg) {
		const res = await git().pull(remote, 'master');
		if (res.summary.changes === 0) return msg.responder.info('Already up to date.');
		return msg.responder.success(`Update complete. ${res.summary.insertions} insertions, ${res.summary.deletions} deletions.`);
	}

};

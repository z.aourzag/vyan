const { Command, RichDisplay, util } = require('klasa');
const { MessageEmbed, Permissions } = require('discord.js');

const PERMISSIONS_RICHDISPLAY = new Permissions([Permissions.FLAGS.MANAGE_MESSAGES, Permissions.FLAGS.ADD_REACTIONS]);
const time = 1000 * 60 * 3;

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			description: language => language.get('COMMAND_HELP_DESCRIPTION'),
			usage: '(Command:command)',
			permissionLevel: 10
		});

		this.createCustomResolver('command', (arg, possible, message) => {
			if (!arg || arg === '') return undefined;
			return this.client.arguments.get('command').run(arg, possible, message);
		});

		this.handlers = new Map();
	}

	async run(message, [command]) {
		const method = this.client.user.bot ? 'author' : 'channel';
		if (command) {
			const info = [
				`───── ${command.name} ─────`,
				util.isFunction(command.description) ? command.description(message.language) : command.description,
				message.language.get('COMMAND_HELP_USAGE', command.usage.fullUsage(message)),
				message.language.get('COMMAND_HELP_EXTENDED'),
				util.isFunction(command.extendedHelp) ? command.extendedHelp(message.language) : command.extendedHelp
			].join('\n');
			return message.sendMessage(info);
		}
		const help = await this.buildHelpDM(message);
		const categories = Object.keys(help);
		const helpMessage = [];
		for (let cat = 0; cat < categories.length; cat++) {
			helpMessage.push(`**───── ${categories[cat]} Commands ─────**`, '\n');
			const subCategories = Object.keys(help[categories[cat]]);
			for (let subCat = 0; subCat < subCategories.length; subCat++) helpMessage.push(`[ ${subCategories[subCat]} ]`, `${help[categories[cat]][subCategories[subCat]].join('\n')}\n`);
			helpMessage.push('\u200b');
		}

		return message[method].send(helpMessage, { split: { char: '\u200b' } })
			.then(() => { if (message.channel.type !== 'dm' && this.client.user.bot) message.sendLocale('COMMAND_HELP_DM'); })
			.catch(async () => {
				await message.channel.send(':x:  :: You have DMs disabled, sending you the commands in an embed.');
				if (message.guild && message.channel.permissionsFor(this.client.user).has(PERMISSIONS_RICHDISPLAY)) {
					// Finish the previous handler
					const previousHandler = this.handlers.get(message.author.id);
					if (previousHandler) previousHandler.stop();

					const handler = await (await this.buildDisplay(message)).run(await message.send('Loading Commands...'), {
						filter: (reaction, user) => user.id === message.author.id,
						time
					});
					handler.on('end', () => this.handlers.delete(message.author.id));
					this.handlers.set(message.author.id, handler);
					return handler;
				}
				return message[method].send(await this.buildHelpEmbed(message), { split: { char: '\n' } })
					.then(() => { if (message.channel.type !== 'dm' && this.client.user.bot) message.sendLocale('COMMAND_HELP_DM'); })
					.catch(() => { if (message.channel.type !== 'dm' && this.client.user.bot) message.sendMessage(':x:  ::  Please allow me to have these permissions so that I can send you my commands list! [`MANAGE_MESSAGES`, `ADD_REACTIONS`, `EMBED_LINKS`]'); }); // eslint-disable-line max-len
			});
	}

	async buildHelpDM(message) {
		const help = {};

		const commandNames = [...this.client.commands.keys()];
		const longest = commandNames.reduce((long, str) => Math.max(long, str.length), 0);

		await Promise.all(this.client.commands.map((command) =>
			this.client.inhibitors.run(message, command, true)
				.then(() => {
					if (!help.hasOwnProperty(command.category)) help[command.category] = {};
					if (!help[command.category].hasOwnProperty(command.subCategory)) help[command.category][command.subCategory] = [];
					const description = typeof command.description === 'function' ? command.description(message.language) : command.description;
					help[command.category][command.subCategory].push(`\`${message.guildSettings.prefix}${command.name.padEnd(longest)}\` → ${description}`);
				})
				.catch(() => {
					// noop
				})
		));

		return help;
	}

	async buildHelpEmbed(message) {
		const commands = await this._fetchCommands(message);
		const { prefix } = message.guildSettings;

		const helpMessage = [];
		for (const [category, list] of commands) {
			helpMessage.push(`**${category} Commands**:\n`, list.map(this.formatCommand.bind(this, message, prefix, false)).join('\n'), '');
		}
		return helpMessage.join('\n');
	}

	async buildDisplay(message) {
		const commands = await this._fetchCommands(message);
		const { prefix } = message.guildSettings;
		const display = new RichDisplay();
		const color = message.member.displayColor;
		for (const [category, list] of commands) {
			display.addPage(new MessageEmbed()
				.setTitle(`${category} Commands`)
				.setColor(color)
				.setDescription(list.map(this.formatCommand.bind(this, message, prefix, true)).join('\n'))
			);
		}

		return display;
	}

	formatCommand(message, prefix, richDisplay, command) {
		const description = typeof command.description === 'function' ? command.description(message.language) : command.description;
		return richDisplay ? `\`${prefix}${command.name}\` → ${description}` : `• **${prefix}${command.name}** → ${description}`;
	}

	async _fetchCommands(message) {
		const run = this.client.inhibitors.run.bind(this.client.inhibitors, message);
		const commands = new Map();
		await Promise.all(this.client.commands.map((command) => run(command, true)
			.then(() => {
				const category = commands.get(command.category);
				if (category) category.push(command);
				else commands.set(command.category, [command]);
			}).catch(() => {
				// noop
			})
		));

		return commands;
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Modifies the family roles.',
			subcommands: true,
			usage: '[add|remove] [rolename:str]',
			usageDelim: ' '
		});
	}

	async run(msg) {
		msg.channel.send({
			embed: {
				fields: [
					{
						name: 'Family Roles',
						value: Object.keys(this.client.settings.get('family')).join('\n')
					}
				],
				color: msg.member.roles.color.color
			}
		});
	}

	async add(msg, [role]) {
		if (this.hasPermissions(msg)) {
			if (!this.client.gateways.clientStorage.schema.family.has(role)) {
				this.client.gateways.clientStorage.schema.family.add(role, 'string', { default: [], array: true });
			} else { return msg.responder.error('That role already exists.'); }
		} else { return msg.responder.error("You don't have the permission to do that."); }
		return msg.reactor.success();
	}

	async remove(msg, [role]) {
		if (this.hasPermissions(msg)) {
			if (this.client.gateways.clientStorage.schema.family.has(role)) {
				this.client.gateways.clientStorage.schema.family.remove(role);
			} else { return msg.responder.error("That role doesn't exist."); }
		} else { return msg.responder.error("You don't have the permission to do that."); }
		return msg.reactor.success();
	}

	async hasPermissions(msg) {
		if (msg.author.id === this.client.owner.id || msg.author.id === '305407776199540736') return true;
		else return false;
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Broadcasts an announcement on all guilds.',
			usage: '<title:string> <content:string>',
			usageDelim: ';;',
			requiredPermissions: ['SEND_MESSAGES', 'EMBED_LINKS']
		});
	}

	async run(msg, [title, content]) {
		for (const guild of this.client.guilds) {
			const _channel
				= guild[1].settings.announcementChannel
				|| guild[1].channels.find(channel => channel.name === 'announcements'
				|| channel.name === 'news'
				|| channel.name === 'info')
				|| guild[1].channels.first();
			await _channel.send({ embed: {
				title: title,
				description: content,
				footer: {
					text: `official announcement by ${msg.author.tag}`
				}
			} }).catch(() => null);
		}
	}

};

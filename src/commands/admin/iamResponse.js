const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Configure the I am response for this guild.',
			permissionLevel: 10,
			runIn: ['text'],
			usage: '[enable|disable]',
			usageDelim: ' '
		});
	}

	async run(msg, [selector]) {
		switch (selector) {
			case 'enable':
				await msg.guild.settings.update('iamResponse', true);
				return msg.reactor.success();
			case 'disable':
				await msg.guild.settings.update('iamResponse', false);
				return msg.reactor.success();
			default:
				if (msg.guild.settings.iamResponse) {
					await msg.guild.settings.update('iamResponse', false);
					return msg.responder.success('Successfully disabled the I am response.');
				} else {
					await msg.guild.settings.update('iamResponse', true);
					return msg.responder.success('Successfully enabled the I am response.');
				}
		}
	}

};

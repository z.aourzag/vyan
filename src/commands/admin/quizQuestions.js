/* eslint-disable id-length */
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 3,
			runIn: ['text'],
			aliases: ['qq'],
			description: 'Modifies questions of the quiz.',
			usage: '<add|remove|list> [id:int]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async add(msg) {
		var question = '';
		var answers = [];
		try {
			msg.channel.send('What should the question be?');
			const collectedQ = await msg.channel.awaitMessages(mes => msg.author.id === mes.author.id, { max: 1, time: 60000, errors: ['time'] });
			question = collectedQ.first().content;
		} catch (_) {
			return msg.responder.error("Didn't specify a question in time. Try again.");
		}
		try {
			msg.channel.send('Which are the correct answers (divided by a `;`)?');
			const collectedA = await msg.channel.awaitMessages(mes => msg.author.id === mes.author.id, { max: 1, time: 1200000, errors: ['time'] });
			answers = collectedA.first().content.split(';');
		} catch (_) {
			return msg.responder.error("Didn't specify any answers in time. Try again.");
		}
		await this.client.settings.update('quiz', { q: question, a: answers }, { action: 'add' });
		return msg.channel.send({
			embed: {
				author: {
					name: 'Question added.'
				},
				fields: [
					{
						name: 'question',
						value: question
					},
					{
						name: 'correct answers',
						value: answers.join(', ')
					}
				]
			}
		});
	}

	async remove(msg, [id]) {
		if (!id) return msg.channel.send('Whoops; you didn\'t specify which question to remove.');
		await this.client.settings.update('quiz', this.client.settings.quiz[id - 1], { action: 'remove' });
		return msg.responder.success(`Removed question \`${this.client.settings.quiz[id - 1].q}\`.`);
	}

	async list(msg) {
		const message = [];
		let i = 0;
		for (const item of this.client.settings.quiz) {
			i++;
			message.push(`${i}. ${item.q}`);
		}

		msg.channel.send({ embed: {
			title: 'Quiz Questions',
			description: message.join('\n') || 'none'
		} });
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 3,
			aliases: ['stm'],
			requiredPermissions: ['MANAGE_ROLES'],
			runIn: ['text'],
			description: 'Mutes a mentioned user on all synced guilds.',
			usage: '<member:member> <how_long:time> [reason:string] [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [member, time, ...reason]) {
		if (this.client.settings.syncedAdmins.includes(member.user.id)) return msg.responder.error('You cannot mute a syncAdmin.');
		if (member.user.id === msg.author.id) return msg.responder.error('Why would you mute yourself?');
		if (member.user.id === this.client.user.id) return msg.responder.error('Have I done something wrong?');

		reason = reason.length ? reason.join(' ') : 'no reason specified';
		reason = `Mute by ${msg.author.tag} | ${reason} (unmute at ${time})`;
		for (const guild of this.client.settings.syncedGuilds) {
			const _guild = this.client.guilds.get(guild);
			await _guild.members.fetch(member).catch(() => null);
			const _member = _guild.members.get(member).catch(() => null);
			if (!_member) continue;
			_member.roles.add(_guild.settings.muteRole, reason).catch(() => null);
		}

		await this.client.schedule.create('synctempunmute', time, {
			data: {
				user: member.user.id
			}
		});

		await msg.guild.logger.syncTempMute({ user: member.user, reason, time, moderator: msg.author });
		return msg.reactor.success();
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 3,
			description: 'Kick a user on all synced guilds.',
			usage: '<user:user> [reason:str] [...]',
			usageDelim: ' ',
			aliases: ['sk'],
			requiredPermissions: ['KICK_MEMBERS']
		});
	}

	async run(msg, [user, ...reason]) {
		if (this.client.settings.syncedAdmins.includes(user.id)) return msg.reactor.error();
		if (!reason || !reason.length) {
			reason = 'no reason provided';
		} else {
			reason = reason.join(' ');
		}
		const syncedGuilds = this.client.settings.get('syncedGuilds');
		if (!syncedGuilds.length) return msg.responder.error('There are no synced guilds.');
		await this.client.users.fetch(user.id);
		const kickedOn = [];
		const notKickedOn = [];
		for (let i = 0; i < syncedGuilds.length; i++) {
			if (this.client.guilds.get(syncedGuilds[i]).members.get(user.id) === undefined) {
				notKickedOn.push(this.client.guilds.get(syncedGuilds[i]).name);
			} else if (this.client.guilds.get(syncedGuilds[i]).members.get(user.id).kickable) {
				await this.client.guilds.get(syncedGuilds[i]).members.get(user.id).kick();
				kickedOn.push(this.client.guilds.get(syncedGuilds[i]).name);
			} else {
				notKickedOn.push(this.client.guilds.get(syncedGuilds[i]).name);
			}
		}

		if (notKickedOn.length) {
			await msg.channel.send({ embed: {
				title: `${this.client.users.get(user.id).tag} has not been kicked from:`,
				description: `❯ ${notKickedOn.join('\n❯ ')}`,
				color: 16007990
			} });
		}

		await msg.guild.logger.syncKick({ user,	reason,	moderator: msg.author });
		if (kickedOn.length) return msg.reactor.kick();
		return msg.reactor.error();

		/*
    	 * msg.channel.send((`**${this.client.users.get(user.id).tag}** has been kicked from:\n\n❯ ${kickedOn.length > 0 ? kickedOn.join('\n❯ ') : 'nowhere'}`) +
    	 * (notKickedOn.length > 0 ? `\n\n**${this.client.users.get(user.id).tag}** has **not** been kicked from:\n\n❯ ${notKickedOn.join('\n❯ ')}` : ''));
    	 */
	}

};

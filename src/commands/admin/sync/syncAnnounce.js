const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 3,
			description: 'Broadcasts an announcement on all synced guilds.',
			usage: '[title:string] [content:string]',
			usageDelim: ';;',
			aliases: ['sa'],
			requiredPermissions: ['SEND_MESSAGES', 'EMBED_LINKS']
		});
	}

	async run(msg, [title, content]) {
		for (const guild of this.client.settings.syncedGuilds) {
			const _guild = this.client.guilds.get(guild);
			const _channel = _guild.channels.get(_guild.settings.announcementChannel);
			await _channel.send({ embed: {
				title: title,
				description: content,
				footer: {
					text: `official announcement by ${msg.author.tag}`
				}
			} }).catch(() => null);
		}
		return msg.reactor.success();
	}

};

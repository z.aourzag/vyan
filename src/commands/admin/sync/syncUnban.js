const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 3,
			description: 'Unban a user on all synced guilds.',
			usage: '<user:user> [reason:string] [...]',
			usageDelim: ' ',
			aliases: ['sub'],
			requiredPermissions: ['BAN_MEMBERS']
		});
	}

	async run(msg, [user, ...reason]) {
		if (!reason || !reason.length) {
			reason = 'No reason provided';
		} else {
			reason = reason.join(' ');
		}
		const syncedGuilds = this.client.settings.get('syncedGuilds');
		if (!syncedGuilds.length) return msg.responder.error('There are no synced guilds.');
		await this.client.users.fetch(user.id);
		const unbannedOn = [];
		for (let i = 0; i < syncedGuilds.length; i++) {
			if (this.client.guilds.get(syncedGuilds[i]).me.hasPermission('BAN_MEMBERS')) {
				await this.client.guilds.get(syncedGuilds[i]).members.unban(user.id, `${msg.author.tag} | ${reason}`);
				unbannedOn.push(this.client.guilds.get(syncedGuilds[i]).name);
			} else {
				await msg.responder.error(`I'm missing (un)ban permission on ${this.client.guilds.get(syncedGuilds[i]).name}.`);
			}
		}
		await msg.guild.syncUnban({ user, reason, moderator: msg.author });
		await this.client.settings.update('syncBannedUsers', user.id, { action: 'remove' });
		return msg.responder.success(`**${this.client.users.get(user.id).tag}** has been unbanned from:\n\n❯ ${unbannedOn.length ? unbannedOn.join('\n❯ ') : 'nowhere'}`);
	}

};

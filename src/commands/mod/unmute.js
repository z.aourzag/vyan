const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['um', 'release'],
			requiredPermissions: ['MANAGE_ROLES'],
			runIn: ['text'],
			description: 'Unmutes a mentioned user. Currently does not require reason (no mod-log).',
			usage: '<member:member> [reason:string] [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [member, ...reason]) {
		await msg.guild.members.fetch(member).catch(() => null);
		// if ((!reason || !reason.length) && msg.guild.settings.get('reasons')) return msg.responder.error('Reasons are required by the administration on this server.');

		reason = reason.length ? reason.join(' ') : 'no reason specified';
		reason = `Unmute by ${msg.author.tag} | ${reason}`;

		await member.roles.remove(msg.guild.settings.muteRole, reason).catch(() => msg.responder.error("Mute hasn't been initialized yet or I don't have permission to assign the muterole."));
		await msg.guild.logger.unmute({
			user: member.user,
			reason,
			moderator: msg.author
		});
		await member.settings.update('muted', false);

		return msg.reactor.unmute();
	}

};

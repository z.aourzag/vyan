/* eslint-disable */
const { Command, util } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			description: "Checks the guild for any users banned on KSoft.Si's global ban list."
		});
	}

	async run(msg) {
		
	}

};

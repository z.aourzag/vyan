const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['ran'],
			runIn: ['text'],
			usage: '[reason:str]',
			description: 'Resets all nicknames in the current guild.'
		});
		this.guilds = new Set();
	}

	async run(msg, [reason]) {
		if (this.guilds.has(msg.guild.id)) return msg.responder.error('One reset instance is already running.');
		this.guilds.add(msg.guild.id);
		for (const member of msg.guild.members) {
			await member[1].setNickname('').catch(() => null);
		}
		this.guilds.remove(msg.guild.id);
		await msg.guild.logger.resetAllNicknames({
			user: msg.author,
			reason: reason || 'no reason provided'
		});
		return msg.reactor.success();
	}

};

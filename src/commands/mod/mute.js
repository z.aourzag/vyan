const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['m', 'timeout'],
			runIn: ['text'],
			description: 'Mutes a mentioned user.',
			usage: '<member:member> [duration:time] [reason:string] [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [member, time, ...reason]) {
		if (member.user.id === msg.author.id) return msg.responder.error('Why would you mute yourself?');
		if (member.user.id === this.client.user.id) return msg.responder.error('Have I done something wrong?');
		// if ((!reason || !reason.length) && msg.guild.settings.get('reasons')) return msg.responder.error('Reasons are required by the administration on this server.');
		let { muteRole } = msg.guild.settings;
		if (!muteRole) muteRole = await this.setup(msg);
		await msg.guild.members.fetch(member).catch(() => null);

		reason = reason.length ? reason.join(' ') : 'no reason specified';
		reason = `Mute by ${msg.author.tag} | ${reason}`;

		await member.roles.add(muteRole, reason).catch(() => msg.responder.error("I don't have permission to assign the muterole."));
		if (time) {
			await this.client.schedule.create('tempunmute', time, {
				data: {
					guild: msg.guild.id,
					user: member.user.id
				}
			});
		}

		await msg.guild.logger.mute({
			time,
			user: member.user,
			reason,
			moderator: msg.author
		});
		await member.settings.update('muted', true);
		return msg.reactor.mute();
	}

	async setup(msg) {
		const muterole = await msg.guild.roles.create({
			data: {
				name: 'Silenced',
				permissions: 0
			},
			reason: 'Initializing mute-command.'
		});

		for (const channel of msg.guild.channels.values()) {
			channel.updateOverwrite(
				muterole.id,
				{
					SEND_MESSAGES: false,
					ATTACH_FILES: false,
					ADD_REACTIONS: false
				},
				'Initializing mute-command'
			);
		}
		await msg.guild.settings.update('muteRole', muterole.id, msg.guild);

		return muterole.id;
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['b', '410', 'bean'],
			requiredPermissions: ['BAN_MEMBERS'],
			runIn: ['text'],
			description: 'Bans a mentioned user or ID. Specify a duration to temp- or use --soft to softban.',
			usage: '<member:user> [duration:time] [reason:string] [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [user, time, ...reason]) {
		if (user.id === msg.author.id) return msg.responder.error('Why would you ban yourself?');
		if (user.id === this.client.user.id) return msg.responder.error('Have I done something wrong?');
		// if ((!reason || !reason.length) && msg.guild.settings.get('reasons')) return msg.responder.error('Reasons are required by the administration on this server.');

		const soft = Boolean(msg.flags.soft || msg.flags.softban);

		if (soft && time) return msg.responder.error("Can't temp- and softban at the same time.");

		const member = await msg.guild.members.fetch(user).catch(() => null);
		if (member) {
			if (member.roles.highest.position >= msg.member.roles.highest.position && msg.author.id !== msg.guild.owner.id) return msg.responder.error('You cannot ban this user.');
			if (!member.bannable) return msg.responder.error('I cannot ban this user.');
		}

		const options = {};
		reason = reason.length ? reason.join(' ') : 'no reason specified';
		options.reason = `${msg.author.tag} | ${reason}`;
		if (soft) options.days = msg.flags.days || 1;

		await msg.guild.members.ban(user, options);
		await msg.guild.logger.ban({
			time,
			soft,
			user,
			reason,
			moderator: msg.author
		});

		if (soft) msg.guild.members.unban(user, 'Softban released.');

		if (time) {
			await this.client.schedule.create('tempunban', time, {
				data: {
					guild: msg.guild.id,
					user: user.id
				}
			});
		}

		return msg.reactor.ban();
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['modlog'],
			runIn: ['text'],
			description: 'Toggle requiring reasons for moderative actions.',
			usage: '[enable|disable]'
		});
	}

	async run(msg, [option]) {
		if (option === 'enable') {
			await msg.guild.settings.update('reasons', true);
		} else if (option === 'disable') {
			await msg.guild.settings.update('reasons', false);
		} else if (!option || !option.length) {
			if (msg.guild.settings.get('reasons')) await msg.guild.settings.update('reasons', false);
			else await msg.guild.settings.update('reasons', true);
		}
		return msg.reactor.success();
	}

};

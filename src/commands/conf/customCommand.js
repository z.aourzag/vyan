const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['cc'],
			runIn: ['text'],
			description: 'Manage CustomCommands',
			usage: '<add|remove|view|list:default> [name:str] [command:str] [...]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async add(msg, [name, ...command]) {
		if (!name) return msg.responder.error('No name specified.');
		if (!command || !command.length) return msg.responder.error('You need to enter a command text.');
		command = command.join(' ');
		name = name.toLowerCase();
		const cmd = msg.guild.settings.customCommands.find(cm => cm.name === name);
		if (cmd) return msg.responder.error('That command already exists.');
		await msg.guild.settings.update('customCommands', { name, command }, { action: 'add' });
		return msg.reactor.success();
	}

	async remove(msg, [name]) {
		if (!name) return msg.responder.error('No CustomCommand specified.');
		const cmd = msg.guild.settings.customCommands.find(cm => cm.name === name.toLowerCase());
		if (!cmd) return msg.responder.error("That command couldn't be found.");
		await msg.guild.settings.update('customCommands', cmd, { action: 'remove' });
		return msg.reactor.success();
	}

	async list(msg) {
		if (!msg.guild.settings.customCommands.length) return msg.responder.error("There haven't been any customCommands added yet.");
		return msg.responder.info(msg.guild.settings.customCommands.map(cmd => cmd.name).join('\n'));
	}

	async view(msg, [name]) {
		if (!name) return msg.responder.error('No CustomCommand specified.');
		const cmd = msg.guild.settings.customCommands.find(cm => cm.name === name.toLowerCase());
		if (!cmd) return msg.responder.error('Unknown CustomCommand.');
		return msg.responder.info(`\`${cmd.command}\``);
	}

};

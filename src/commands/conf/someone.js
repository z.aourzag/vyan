const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			description: 'Toggle @someone.',
			usage: '<enable|disable>'
		});
	}

	async run(msg, [toggle]) {
		toggle === 'enable'
			? await msg.guild.settings.reset('disableSomeone')
			: await msg.guild.settings.update('disableSomeone', true);
		return msg.reactor.success();
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['wel', 'wc'],
			runIn: ['text'],
			description: 'Sets up a welcome message.',
			usage: '<set|enable|disable> [channel:channel] [message:str] [...]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async set(msg, [channel, ...text]) {
		if (!channel) ({ channel } = msg);
		if (!text || !text.length) return msg.responder.error('You need to specify a message to send.');
		await msg.guild.settings.update('welcome.message', text.join(' '));
		await msg.guild.settings.update('welcome.channel', channel);
		return msg.responder.success(`Enabled welcome message in ${channel.toString()}.`);
	}

	async enable(msg) {
		await msg.guild.settings.update('welcome.enabled', true);
		return msg.reactor.success();
	}

	async disable(msg) {
		await msg.guild.settings.update('welcome.enabled', false);
		return msg.reactor.success();
	}

};

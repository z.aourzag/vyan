const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			description: 'Setup starboard.',
			usage: '<starChannel:channel>'
		});
	}

	async run(msg, [channel]) {
		await msg.guild.settings.update('starChannel', channel, { action: 'add' });
		return msg.reactor.success();
	}

};


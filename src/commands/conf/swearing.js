const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['filter', 'addfilter', 'swear', 'addswearing'],
			runIn: ['text'],
			description: 'Adds a word to the list of filtered words.',
			usage: '<filter:string>'
		});
	}

	async run(msg, [filter]) {
		await msg.guild.settings.update('antispam.settings.bannedWords', filter);
		if (msg.guild.settings.antispam.settings.bannedWords.includes(filter)) {
			return msg.responder.success(`\`${filter}\` has been **added** to the list of filtered words.`);
		} else {
			return msg.responder.success(`\`${filter}\` has been **removed** from the list of filtered words.`);
		}
	}

};

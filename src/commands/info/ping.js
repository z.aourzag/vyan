const { Command } = require('klasa');
const aliases = ['Pang', 'Peng', 'Pong', 'Pung', 'Pyng'];

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			aliases: aliases.map(alias => alias.toLowerCase()),
			description: language => language.get('COMMAND_PING_DESCRIPTION')
		});
	}

	async run(message) {
		return message.responder.info(`${this.getRandomAlias()}! ${Math.floor(this.client.ws.ping)}ms`);
	}

	getRandomAlias() {
		return aliases[Math.floor(Math.random() * aliases.length)];
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			guarded: true,
			description: language => language.get('COMMAND_INVITE_DESCRIPTION')
		});
	}

	async run(msg) {
		const invite = `https://discordapp.com/oauth2/authorize?client_id=${this.client.application.id}&scope=bot&permissions=${msg.flags.adminonly ? '8' : '503705087'}`;
		return msg.responder.info(`**[Invite ${this.client.user.username}](${invite})**`);
	}

	async init() {
		if (this.client.application && !this.client.application.botPublic) this.permissionLevel = 10;
	}

};

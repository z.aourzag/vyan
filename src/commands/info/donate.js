const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			description: 'Get the donation link. Duh.',
			aliases: ['don']
		});
	}

	async run(msg) {
		msg.responder.info([
			'**[Please donate to keep Vyan going c:](https://www.patreon.com/join/thawhiteraven)**',
			'',
			'Just want to do me a good thing and **[support one-time](https://ko-fi.com/thawhiteraven)**? Sounds good! ^°^'
		].join('\n'));
	}

};

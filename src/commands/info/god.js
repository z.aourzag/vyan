const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			cooldown: 4,
			description: 'Oh my god.'
		});
	}

	async run(msg) {
		msg.responder.info(this.client.owner.tag);
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['amigay', 'amigay?', 'areyougay?'],
			description: 'Are you gay?',
			usage: '[user:user]'
		});
	}

	async run(msg, [user]) {
		const gay = this.getRandomBool();
		if (gay) {
			return msg.reply(`${user ? `${user.username} is` : 'you are'} **gay**.`);
		} else {
			return msg.reply(`${user ? `${user.username} is` : 'you are'} **not gay**.`);
		}
	}

	getRandomBool() {
		return Boolean(Math.round(Math.random()));
	}

};

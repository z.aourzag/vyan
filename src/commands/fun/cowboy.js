const cowboy = '567793630488690709';
const sadboy = '527173284425629696';

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'YEEEE HAWWW.'
		});
	}

	async run(msg) {
		if (msg.flags.sad) return msg.channel.send('Yee. Haw?').then(m => m.react(sadboy));
		return msg.channel.send('YEE-HAW.').then(m => m.react(cowboy));
	}

};

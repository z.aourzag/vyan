const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'The one and only change my mind meme. Change my mind.',
			usage: '<text:str>',
			cooldown: 5
		});
	}

	async run(msg, [text]) {
		const { message: img } = await get('https://nekobot.xyz/api/imagegen').query({ type: 'changemymind', text }).send().then(r => r.json());
		return msg.responder.image(img);
	}

};

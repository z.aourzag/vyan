const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Oh wow.',
			usage: '<user:user>',
			cooldown: 5
		});
	}

	async run(msg, [user]) {
		const { message: img } = await get('https://nekobot.xyz/api/imagegen')
			.query({
				type: 'kidnap',
				image: user.displayAvatarURL({ format: 'png', size: 2048 })
			})
			.send()
			.then(r => r.json());
		return msg.responder.image(img);
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'File a divorce.',
			cooldown: 43200
		});
	}

	async run(msg) {
		if (!msg.author.settings.marriedTo) return msg.responder.error("You can't divorce anyone if you're not married o.O");
		if (await msg.ask(`Are you sure you want to divorce ${this.client.users.get(msg.author.settings.marriedTo).username}?`)) {
			const marriedToName = this.client.users.get(msg.author.settings.marriedTo).username;
			await this.client.users.get(msg.author.settings.marriedTo).settings.reset('marriedTo');
			await msg.author.settings.reset('marriedTo');
			return msg.responder.error(`💔 ${msg.author.username} divorced ${marriedToName}.`);
		}
		return msg.responder.error("Didn't divorce.");
	}

};

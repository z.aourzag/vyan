const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Poke someone! Ouchie.',
			usage: '<somebody:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [user]) {
		let self;
		if (user.id === msg.author.id) self = true;
		const { url } = await get('https://nekos.life/api/v2/img/poke').send().then(r => r.json());
		msg.channel.send({
			embed: {
				description: self ? `${user} poked themselves. Why.` : `${msg.author} poked ${user}`,
				image: {
					url
				}
			}
		});
	}

};

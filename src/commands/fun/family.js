const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			description: 'Shows the family.',
			subcommands: true,
			usage: '[add|remove] [user:str] [identifier:str]',
			usageDelim: ' '
		});
	}

	async run(msg) {
		const embed = new MessageEmbed()
			.setAuthor('The Family')
			.setColor(msg.member.roles.color.color);
		for (let i = 0; i < Object.keys(this.client.settings.family).length; i++) {
			embed.addField(Object.keys(this.client.settings.family)[i],
				this.client.settings.get(`family.${Object.keys(this.client.settings.family)[i]}`).length
					? this.client.settings.get(`family.${Object.keys(this.client.settings.family)[i]}`).join('\n')
					: 'noone',
				true
			);
		}
		return msg.channel.send(embed);
	}

	async add(msg, [user, identifier]) {
		const familyroles = Object.keys(this.client.settings.family);
		if (!user || !familyroles.includes(identifier)) return msg.reactor.error();
		if (this.getPermissions(msg) < 2) return msg.reactor.error();
		if (identifier === 'parent') identifier = 'parents';
		if (identifier === 'child') identifier = 'children';
		await this.client.settings.update(`family.${identifier}`, user, { action: 'add' });
		return msg.reactor.success();
	}

	async remove(msg, [user]) {
		if (!user) return msg.reactor.error();
		if (this.getPermissions(msg) < 2) return msg.reactor.error();
		const familyroles = Object.keys(this.client.settings.family);
		for (let i = 0; i < familyroles.length; i++) {
			await this.client.settings.update(`family.${familyroles[i]}`, user, { action: 'remove' });
		}
		return msg.reactor.success();
	}

	async getPermissions(msg) {
		if (msg.author.id === this.client.owner.id) return 3;
		else if (msg.author.id === '305407776199540736') return 2;
		else return 0;
	}

};

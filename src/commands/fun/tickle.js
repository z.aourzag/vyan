const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Tickle someone! Don\'t stop. Never stop.',
			usage: '<somebody:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [user]) {
		let self;
		if (user.id === msg.author.id) self = true;
		const { url } = await get('https://nekos.life/api/v2/img/tickle').send().then(r => r.json());
		msg.channel.send({
			embed: {
				description: self ? `${user} tickled themselves. How awkward.` : `${msg.author} tickled ${user}`,
				image: {
					url
				}
			}
		});
	}

};

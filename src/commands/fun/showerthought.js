const { Command } = require('klasa');
const { WebhookClient } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['thonk'],
			description: 'Suggests a shower thought.',
			usage: '<approve|decline|list|view|add:default> [thought:str] [...]',
			subcommands: true,
			usageDelim: ' '
		});

		this.hook = new WebhookClient(process.env.THINK_ID, process.env.THINK_TOKEN);
	}

	async add(msg, [...thought]) {
		thought = thought.join(' ');
		if (!thought || !thought.length) return msg.responder.error('Invalid thought.');

		const id = new Date().getTime().toString(36);

		await this.client.settings.update('showerThoughts', { thought, id }, { action: 'add' });

		return msg.responder.success(`Your shower thought has been successfully requested for approval with id \`${id}\`.`);
	}

	async approve(msg, [id]) {
		const thought = await this.find(id);
		if (!thought) return msg.responder.error('Invalid ID.');
		if (!this.checkPermissions(msg)) return msg.responder.error("You don't have permission to do this.");

		await this.hook.send(thought.thought);
		await this.client.settings.update('showerThoughts', thought, { action: 'remove' });
		return msg.reactor.success();
	}

	async decline(msg, [id]) {
		const thought = await this.find(id);
		if (!thought) return msg.responder.error('Invalid ID.');
		if (!this.checkPermissions(msg)) return msg.responder.error("You don't have permission to do this.");

		await this.client.settings.update('showerThoughts', thought, { action: 'remove' });
		return msg.reactor.success();
	}

	async list(msg) {
		if (!this.checkPermissions(msg)) return msg.responder.error("You don't have permission to do this.");
		return msg.responder.info(this.client.settings.showerThoughts
			? this.client.settings.showerThoughts
				.map(th => `\`${th.id}\` ${th.thought.substring(0, 20)}${th.thought.length > 20 ? '...' : ''}`)
				.join('\n')
			: 'none');
	}

	async view(msg, [id]) {
		const thought = await this.find(id);
		if (!thought) return msg.responder.error('Invalid ID.');
		if (!this.checkPermissions(msg)) return msg.responder.error("You don't have permission to do this.");
		return msg.responder.info(`Thought #${id}`, thought.thought);
	}

	// helper functions
	async find(id) {
		return this.client.settings.showerThoughts.find(th => th.id === id);
	}

	async checkPermissions(msg) {
		return msg.hasAtLeastPermissionLevel(1);
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Claim 150-250 daily credits.',
			aliases: ['dailies'],
			cooldown: 86400
		});
	}

	async run(msg) {
		const amount = this.getAmount(150, 100);
		await msg.author.settings.update('credits', msg.author.settings.credits + amount);
		return msg.responder.success(`Received your **${amount}** daily credits. Your new balance is **${msg.author.settings.credits.toLocaleString()} credits.**`);
	}

	getAmount(startingValue, bonusValue) {
		return startingValue + Math.round(Math.random() * bonusValue);
	}

};

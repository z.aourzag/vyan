/* eslint-disable no-mixed-operators */
const { Command } = require('klasa');
const { Canvas } = require('canvas-constructor');
const fsn = require('fs-nextra');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Displays the public profile of a user or yourself.',
			usage: '[user:username]',
			cooldown: 5
		});
	}

	async run(msg, [user = msg.author]) {
		const image = await fsn.readFile(`${process.cwd()}/assets/profile.jpg`);
		const { body: buffer } = await get(user.displayAvatarURL({ format: 'png', size: 128 })).send();
		const married = this.client.users.get(user.settings.marriedTo) ? this.client.users.get(user.settings.marriedTo).tag : 'noone';
		const canvas = new Canvas(400, 400)
			// background
			.addImage(image, 0, 0, 400, 400)
			// username
			.setColor('#FFFFFF')
			.setTextFont('40px "Fira Code"')
			.addResponsiveText(user.username.replace(/[^\w ]/g, '').replace(/(\s)+/g, ' '), 30, 60, 250)
			// discriminator
			.setTextSize('20px')
			.setColor('#AAAAAA')
			.addResponsiveText(`#${user.discriminator}`, 30, 90)
			// level
			.setTextFont('25px "Fira Code"')
			.addResponsiveText('level', 30, 175)
			.setColor('#FFFFFF')
			.setTextFont('40px "Fira Code"')
			.addResponsiveText(user.settings.level.toString(), 120, 175)
			// xp
			.addResponsiveText(`(${user.settings.experience - (user.settings.level * 10) ** 2}/${((user.settings.level * 2) + 1) * 100} xp)`, 30, 200, 140)
			// rep
			.setColor('#FFFFFF')
			.setTextFont('40px "Fira Code"')
			.setTextAlign('right')
			.addResponsiveText(user.settings.reputation.toString(), 280, 175, 70)
			.setColor('#AAAAAA')
			.setTextAlign('left')
			.setTextFont('25px "Fira Code"')
			.addResponsiveText('rep', 295, 175)
			// credits
			.setColor('#FFFFFF')
			.setTextFont('35px "Fira Code"')
			.addResponsiveText(user.settings.credits.toLocaleString(), 30, 310, 150)
			.setColor('#AAAAAA')
			.setTextFont('20px "Fira Code"')
			.addResponsiveText('credits', 30, 340)
			// marriage
			.setColor('#AAAAAA')
			.setTextFont('20px "Fira Code"')
			.addResponsiveText('married to', 230, 310)
			.setColor('#FFFFFF')
			.setTextFont('40px "Fira Code"')
			.addResponsiveText(married, 230, 340, 140)
			// avatar
			.addRect(25, 110, 350, 2);
		canvas.addCircularImage(buffer, 330, 45, 30);
		return msg.channel.sendFile(canvas.toBuffer());
	}

};

const { Command } = require('klasa');
const { Canvas } = require('canvas-constructor');
const fsn = require('fs-nextra');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Displays current experience and level of a user or yourself.',
			usage: '[user:user]',
			aliases: ['exp', 'points', 'level'],
			cooldown: 5
		});
	}

	async run(msg, [user = msg.author]) {
		const image = await fsn.readFile(`${process.cwd()}/assets/xp.jpg`);
		const { body: buffer } = await get(user.displayAvatarURL({ format: 'png', size: 128 })).send();
		const { level, experience } = user.settings;
		const currentLevelXP = experience - (level * 10) ** 2;
		const nextLevelXP = ((level * 2) + 1) * 100;
		const canvas = new Canvas(400, 200)
			// background
			.addImage(image, 0, 0, 400, 200)
			// username
			.setColor('#FFFFFF')
			.setTextFont('50px "Fira Code"')
			.addResponsiveText(user.username.replace(/[^\w ]/g, '').replace(/(\s)+/g, ' '), 30, 60, 250)
			// level
			.setTextAlign('right')
			.setTextFont('25px "Fira Code"')
			.addResponsiveText('lv.', 295, 170)
			.setColor('#FFFFFF')
			.setTextFont('60px "Fira Code"')
			.addResponsiveText(level.toString(), 370, 170, 70)
			// xp
			.setTextAlign('left')
			.setColor('#AAAAAA')
			.setTextFont('15px "Fira Code"')
			.addResponsiveText(`(${currentLevelXP}/${nextLevelXP} xp)`, 30, 165, 140);
		canvas.setColor('#FFFFFF').addBeveledRect(30, 130, 250, 10);
		canvas.setColor('#111111').addBeveledRect(32, 131, 246 * (currentLevelXP / nextLevelXP), 8);
		canvas.setColor('#FFFFFF').addRect(30, 80, 250, 2);
		canvas.addCircularImage(buffer, 330, 45, 30);
		return msg.channel.sendFile(canvas.toBuffer());
	}

};

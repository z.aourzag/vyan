const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Generates a random number between min and max',
			usage: '[min:integer] [max:integer]',
			usageDelim: ' '
		});
	}

	async run(msg, [min, max]) {
		if (min > max) {
			const minOld = min;
			min = max;
			max = minOld;
		}
		if (min && !max) {
			max = min;
			min = 1;
		} else if (!min && !max) {
			max = 100;
			min = 1;
		}
		Math.round(min);
		Math.round(max);
		const randomInt = this.getRandomInt(min, max);
		msg.responder.success(`Your random number between ${min} and ${max} is **${randomInt}**.`);
	}

	getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['revav'],
			description: 'Get a reverse image search to a user\'s avatar',
			usage: '[user:user]'
		});
	}

	async run(msg, [user]) {
		if (!user) user = msg.author;
		return msg.send({
			embed: {
				description: `**[Search ${user.username}'s Avatar](https://images.google.com/searchbyimage?image_url=${user.displayAvatarURL({ format: 'png', size: 2048 })})**`
			}
		});
	}

};

const { Command } = require('klasa');
const { Collection } = require('discord.js');
const { emojis } = require('../../lib/util/constants');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Checks if a user is in the current server.',
			usage: '<user_id:user> [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [...users]) {
		const mem = await msg.guild.members.fetch().catch(() => null);
		const res = new Collection();
		const ids = users.map(u => u.id);
		for (const id of ids) {
			if (!mem.has(id)) {
				if (!await this.client.users.fetch(id)) {
					res.set(id, {id, exists: false, in: false});
				} else {
					res.set(id, {id, exists: true, in: false});
				}
			} else {
				res.set(id, {id, exists: true, in: true});
			}
		}
		return msg.responder.embed(
			`${res.filter(u => u.in).size}/${res.size} users in guild.`,
			res.map(u => {
				if (!u.exists) return `${emojis.error} ${u.id} Invalid ID`;
				if (!u.in) return `${emojis.error} ${u.id} Not in Guild`;
				return `${emojis.success} ${u.id}`
			}).join('\n'),
			'',
			res.filter(u => !u.in).length ? 'error' : 'success'
		)
	}

};

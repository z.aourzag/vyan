const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Gets a random commit message.',
			aliases: ['commit', 'wtc']
		});
	}

	async run(msg) {
		const commit = await require('centra')('http://whatthecommit.com/index.txt').send().then(r => r.text());
		msg.responder.info(commit);
	}

};

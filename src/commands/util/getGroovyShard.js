/* eslint-disable new-cap */
const { Command } = require('klasa');
const shardCount = 256n;

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 0,
			description: 'Gets the shard id from Groovy provided a guild ID.',
			usage: '[id:str]',
			aliases: ['ggs']
		});
	}

	async run(msg, [id = msg.guild.id]) {
		const guild = this.client.guilds.get(id);
		const val = BigInt(id);
		msg.channel.send({
			embed: {
				description: `${(val >> 22n) % shardCount} / ${shardCount}`,
				author: {
					name: 'Groovy Shard',
					iconURL: this.client.users.get('234395307759108106').avatarURL({ format: 'png' }),
					url: 'https://groovy.bot'
				},
				footer: {
					text: guild ? `${guild.name} (${id})` : id
				}
			}
		});
	}

};

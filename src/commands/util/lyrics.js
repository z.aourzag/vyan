const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');
const get = require('centra');
const baseURL = 'https://api.ksoft.si/lyrics/search';

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Gets lyrics for a song.',
			usage: '<query:str>',
			cooldown: 10
		});
	}

	async run(msg, [query]) {
		const body = await get(baseURL).query({ q: query, limit: 1 }).header('Authorization', `Bearer ${process.env.API_KSOFT}`).send().then(r => r.json());
		if (!body.data) return msg.responder.error('No results found.');
		const res = body.data[0];
		const lyrics = res.lyrics.split('\n');
		while (lyrics.length > 1) {
			const ly = [lyrics[0], lyrics[1]].join('\n');
			if (ly.length < 2000) {
				lyrics.splice(0, 2, ly);
			}
			lyrics[0].replace('\n\n', '\n');
			if (ly.length >= 2000) {
				lyrics[0] += '\n...';
				break;
			}
		}
		const embed = new MessageEmbed()
			.setTitle(`${res.artist} - ${res.name}`)
			.setColor(msg.guild ? msg.guild.me.displayColor : 'RANDOM')
			.setDescription(lyrics[0])
			.setFooter('Powered by KSoft.Si Lyrics');
		return msg.send({ embed });
	}

};

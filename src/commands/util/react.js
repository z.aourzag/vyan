const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			cooldown: 5,
			description: 'Reacts with something.',
			usage: '<reaction:str>'
		});
	}

	async run(msg, [reaction]) {
		msg.addReaction(reaction);
	}

};

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Manage voice channels.',
			subcommands: true,
			usage: '<delete|whitelist|size> [member:member|size:int|reset]',
			usageDelim: ' '
		});
	}

	async delete(msg) {
		const vc = await this.getVC(msg.member);
		if (!vc) return msg.responder.error("You're currently not owner of a temporary voice channel.");

		const vo = msg.guild.settings.voice.channels.find(ch => ch.owner === msg.member.id);
		msg.guild.settings.update('voice.channels', vo, { action: 'remove' });

		vc.delete(`${msg.member.user.tag} requested their temporary channel deleted!`)
			.then()
			.catch(msg.responder.error);

		msg.responder.success(`You have deleted your temporary voice channel.`);
	}

	async whitelist(msg, [member]) {
		if (!(typeof member === 'object')) return;
		const vc = await this.getVC(msg.member);
		if (!vc) return msg.responder.error("You're currently not owner of a temporary voice channel.");
		if (vc.permissionOverwrites.get(member.id)) {
			vc.permissionOverwrites.get(member.id).delete().catch(() => null);
			return msg.responder.success(`You have unwhitelisted ${member.user.tag} from joining **${vc.name}**!`);
		}

		vc.updateOverwrite(member.id, {
			VIEW_CHANNEL: true,
			CONNECT: true
		}).catch(() => null);
		return msg.responder.success(`You have whitelisted ${member.user.tag}. They can now join **${vc.name}**!`);
	}

	async size(msg, [size]) {
		const vc = await this.getVC(msg.member);
		if (!vc) return msg.responder.error("You're currently not owner of a temporary voice channel.");
		if (size === 'reset') size = 0;
		if (![0, 1, 2, 4].includes(size)) return msg.responder.error('Invalid size, you may only set 1, 2, or 4.');
		await vc.setUserLimit(size).catch(() => null);
		return msg.reactor.success();
	}

	async getVC(member) {
		const vc = member.guild.settings.voice.channels.find(ch => ch.owner === member.id);
		if (!vc) return null;
		if (!member.guild.channels.has(vc.id)) return null;
		return member.guild.channels.get(vc.id);
	}

};

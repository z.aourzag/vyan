const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			cooldown: 3,
			aliases: ['wiki'],
			description: 'Finds a Wikipedia Article by title.',
			usage: '<query:str>'
		});
	}

	async run(msg, [query]) {
		const article = await get(`https://en.wikipedia.org/api/rest_v1/page/summary/${query}`)
			.send()
			.then(res => res.json())
			.catch(() => null);
		if (!article) return msg.responder.error("I couldn't find a wikipedia article with that title!");
		const embed = new MessageEmbed()
			.setColor(4886754)
			.setThumbnail((article.thumbnail && article.thumbnail.source) || 'https://i.imgur.com/fnhlGh5.png')
			.setURL(article.content_urls.desktop.page)
			.setTitle(article.title)
			.setDescription(article.extract);
		return msg.sendMessage({ embed });
	}

};

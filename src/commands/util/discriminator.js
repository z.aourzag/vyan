const { Command } = require('klasa');
const fetch = require('node-fetch');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['discrim'],
			description: 'Gives a list of users with a certain discriminator.',
			usage: '<discriminator:str{4,4}>'
		});
	}

	async run(msg, [discrim]) {
		const users = this.client.users.filter(u => u.discriminator === discrim).map(u => u.tag).join('\n');
		if (!users.length) return msg.responder.error('No users with that discriminator.');

		const key = await fetch('https://hasteb.in/documents', { method: 'POST', body: users })
			.then(response => response.json())
			.then(body => body.key);
		return msg.sendMessage(`https://hasteb.in/${key}`);
	}

};

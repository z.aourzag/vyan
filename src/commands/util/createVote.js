const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['SEND_MESSAGES', 'VIEW_CHANNEL', 'READ_MESSAGE_HISTORY', 'EMBED_LINKS'],
			runIn: ['text'],
			aliases: ['crv'],
			description: 'Creates a vote users can participate.',
			usage: '<headline:string> <topic:string>',
			usageDelim: ';;'
		});
	}

	async run(msg, [headline, topic]) {
		msg.send({
			embed: {
				author: {
					name: headline
				},
				color: msg.member.roles.color.color,
				timestamp: Date.now(),
				description: topic,
				footer: {
					text: msg.author.tag,
					iconURL: msg.author.avatarURL({ format: 'png' })
				}
			}
		})
			.then(message => message.reactor.success()
				.then(reaction => reaction.message.reactor.error()));
		return msg.reactor.success();
	}

};

const { Command } = require('klasa');
const pi = require('pi');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Returns up to 2k digits of pi.',
			usage: '[digits:int{1,2000}]',
			cooldown: 2
		});
	}

	async run(msg, [digits = 3]) {
		msg.responder.success(pi(digits));
	}

};

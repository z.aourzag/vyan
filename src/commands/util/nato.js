const { Command } = require('klasa');
const NATO = new Map()
	.set('a', 'ALPHA')
	.set('b', 'BETA')
	.set('c', 'CHARLIE')
	.set('d', 'DELTA')
	.set('e', 'ECHO')
	.set('f', 'FOXTROT')
	.set('g', 'GULF')
	.set('h', 'HOTEL')
	.set('i', 'INDIA')
	.set('j', 'JULIET ')
	.set('k', 'KILO')
	.set('l', 'LIMA')
	.set('m', 'MIKE')
	.set('n', 'NOVEMBER')
	.set('o', 'OSCAR')
	.set('p', 'PAPA')
	.set('q', 'QUEBEC')
	.set('r', 'ROMEO')
	.set('s', 'SIERRA')
	.set('t', 'TANGO')
	.set('u', 'UNIFORM')
	.set('v', 'VICTOR')
	.set('w', 'WHISKEY')
	.set('x', 'X-RAY')
	.set('y', 'YANKEE')
	.set('z', 'ZULU');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			cooldown: 5,
			description: 'Nato up your messages.',
			runIn: ['text'],
			usage: '<text:str>'
		});
	}

	async run(msg, [text]) {
		const message = this.convert(text);
		if (msg.flags.double) return msg.send(this.convert(message)).catch(() => msg.responder.error('Output too long.'));
		if (msg.flags.triple) return msg.send(this.convert(this.convert(message))).catch(() => msg.responder.error('Output too long.'));
		if (msg.flags.quadruple) return msg.send(this.convert(this.convert(this.convert(message)))).catch(() => msg.responder.error('Output too long.'));
		return msg.send(message).catch(() => msg.responder.error('Output too long.'));
	}

	convert(text) {
		let output = '';
		if (!text) return null;
		text = text.toLowerCase();
		for (const char of text) {
			if (/[a-z]/.test(char)) {
				output += NATO.get(char);
			}
			if (/[a-z]|\s/.test(char))output += ' ';
		}
		if (output.length > 2000) return null;
		return output.toTitleCase();
	}

};

/* eslint-disable func-names */
String.prototype.toTitleCase = function () {
	let i, j, str;
	str = this.replace(/([^\W_]+[^\s-]*) */g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());

	const lowers = ['A', 'An', 'The', 'And', 'But', 'Or', 'For', 'Nor', 'As', 'At',
		'By', 'For', 'From', 'In', 'Into', 'Near', 'Of', 'On', 'Onto', 'To', 'With'];
	for (i = 0, j = lowers.length; i < j; i++) {
		str = str.replace(new RegExp(`\\s${lowers[i]}\\s`, 'g'),
			(txt) => txt.toLowerCase());
	}

	const uppers = ['Id', 'Tv'];
	for (i = 0, j = uppers.length; i < j; i++) {
		str = str.replace(new RegExp(`\\b${uppers[i]}\\b`, 'g'),
			uppers[i].toUpperCase());
	}

	return str;
};

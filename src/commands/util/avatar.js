const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['av'],
			description: 'Get the avatar of a user or yourself.',
			usage: '[user:user]'
		});
	}

	async run(msg, [user]) {
		if (!user) user = msg.author;
		msg.send({
			embed: {
				image: {
					url: user.displayAvatarURL({ format: 'png', size: 2048 })
				}
			}
		});
	}


};

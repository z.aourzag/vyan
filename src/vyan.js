const Vyan = require('./lib/Client');
require('./lib/extensions');

new Vyan().login(require('os').platform() === 'win32' ? process.env.DISCORD_TOKEN_CANARY : process.env.DISCORD_TOKEN);

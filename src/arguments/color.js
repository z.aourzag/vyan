const { Argument } = require('klasa');

class Color extends Argument {

	run(arg) {
		const colorMatch = /(?:0x|#)?([A-Fa-f0-9]{6})/.exec(arg);
		if (!colorMatch) throw 'Invalid hex';
		return colorMatch[1];
	}

}

module.exports = Color;

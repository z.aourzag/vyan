Array.prototype.equals = function(arr) {
	const a = [...this].sort(), b = [...arr].sort();
	if (a === b) return true;
	if (a == null || b == null) return false;
	if (a.length != b.length) return false;
	for (let i = 0; i < a.length; ++i) {
		if (a[i] !== b[i]) return false;
	}
	return true;
}

Array.prototype.unique = function() {
	let a = this.concat();
	for (let i=0; i < a.length; ++i) {
		for (let j=i+1; j < a.length; ++j) {
			if (a[i] instanceof Array) {
				if ((a[j] instanceof Array)) {
					if (!a[i].equals(a[j])) continue;
				}
			} else {
				if (a[i] !== a[j]) continue;
			}
			a.splice(j--, 1);
		}
	}
	return a;
};
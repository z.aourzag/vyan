const { Structures } = require('discord.js');

module.exports = Structures.extend('Role', Role => {
	class VyanRole extends Role {

		constructor(...args) {
			super(...args);
			this.customPermissions = this.client.gateways.rolePermissions.create([this.guild.id, this.id]);
		}

		toJSON() {
			return { ...super.toJSON(), customPermissions: this.customPermissions };
		}

	}
	return VyanRole;
});

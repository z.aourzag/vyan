const { Structures } = require('discord.js');

module.exports = Structures.extend('TextChannel', TextChannel => {
	class VyanTextChannel extends TextChannel {

		constructor(...args) {
			super(...args);
			this.customPermissions = this.client.gateways.channelPermissions.create(this.id);
		}

		toJSON() {
			return { ...super.toJSON(), customPermissions: this.customPermissions };
		}

	}

	return VyanTextChannel;
});
